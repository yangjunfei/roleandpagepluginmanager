package com.cbg.dao;

import com.cbg.pojo.RoleBean;
import com.cbg.pojo.UserBean;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chenboge on 2017/5/12.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
@Repository
public interface UserDao {

    public UserBean getUser(Integer id);

    public int insertUser(UserBean bean);

    public int updateUser(UserBean bean);

    public int deleteUser(Integer id);

    public List<UserBean> queryUsersByPage(String userName, RowBounds rowBounds);

}
