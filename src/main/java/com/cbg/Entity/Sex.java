package com.cbg.Entity;

/**
 * Created by chenboge on 2017/5/18.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
//用于SexTypeHandler的性别转换器枚举
public enum Sex {

    MALE(0, "男"),
    FMALE(1, "女");

    //用于保存在数据库
    private int SexCode;
    //用于UI展示
    private String SexName;

    Sex(int sexCode, String sexName) {
        SexCode = sexCode;
        SexName = sexName;
    }

    public int getSexCode() {
        return SexCode;
    }

    public static Sex getSexFromCode(int code) {
        for (Sex sex : Sex.values()) {
            if (sex.getSexCode() == code) {
                return sex;
            }
        }
        return null;
    }
}
