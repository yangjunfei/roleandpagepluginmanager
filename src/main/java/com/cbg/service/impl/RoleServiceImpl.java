package com.cbg.service.impl;

import com.cbg.dao.RoleDao;
import com.cbg.interceptor.PageParam;
import com.cbg.pojo.RoleBean;
import com.cbg.service.RoleService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by chenboge on 2017/5/12.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

    // TODO: 2017/5/12  按类型装配？？还是按名称装配??
    /**
     * @Autowired默认按照类型进行注入
     * @Autowired @Qualifier("personDaoxxx")这样就是按照名称进行装配
     * @Autowired（required=true）必须注入值，不能为null,为false无论注入什么值都是null
     * @Resource（这个注解属于J2EE的），默认安照名称进行装配，名称可以通过name属性进行指定，
     */
    @Autowired
    private RoleDao roleDao;

    //事务：如果存在一个事务，则支持当前事务，如果没有事务则开启
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int insertRole(RoleBean bean) {
        return roleDao.insertRole(bean);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int updateRole(RoleBean bean) {
        return roleDao.updateRole(bean);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int deleteRole(Integer id) {
        return roleDao.deleteRole(id);
    }

    //事务：如果存在一个事务，则支持当前事务，如果没有事务则非事务的执行(没有就不开启事务)
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.SUPPORTS)
    public RoleBean getRole(Integer id) {
        return roleDao.getRole(id);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.SUPPORTS)
    public List<RoleBean> queryRolesByPage(String roleName, int start, int limit) {
        return roleDao.queryRolesByPage(roleName, new RowBounds(start, limit));
    }

    @Override
    public List<RoleBean> queryRoleslimit(int page, int pageSize) {
        PageParam pageParam = new PageParam();
        pageParam.setDefaultPage(page);
        pageParam.setDefaultPageSize(pageSize);
        pageParam.setDefaultUseFlag(true);
        pageParam.setDefaultCheckFlag(true);
        return roleDao.queryRoleslimit(pageParam);
    }
}
