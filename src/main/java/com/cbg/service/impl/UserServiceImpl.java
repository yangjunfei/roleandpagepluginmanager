package com.cbg.service.impl;

import com.cbg.dao.RoleDao;
import com.cbg.dao.UserDao;
import com.cbg.pojo.RoleBean;
import com.cbg.pojo.UserBean;
import com.cbg.service.RoleService;
import com.cbg.service.UserService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by chenboge on 2017/5/12.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    // TODO: 2017/5/12  按类型装配？？还是按名称装配??
    @Autowired
    private UserDao userDao;

    //事务：如果存在一个事务，则支持当前事务，如果没有事务则开启
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int insertUser(UserBean bean) {
        return userDao.insertUser(bean);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int updateUser(UserBean bean) {
        return userDao.updateUser(bean);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int deleteUser(Integer id) {
        return userDao.deleteUser(id);
    }

    //事务：如果存在一个事务，则支持当前事务，如果没有事务则非事务的执行
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.SUPPORTS)
    public UserBean getUser(Integer id) {
        return userDao.getUser(id);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.SUPPORTS)
    public List<UserBean> queryUsersByPage(String userName, int start, int limit) {
        return userDao.queryUsersByPage(userName, new RowBounds(start, limit));
    }

}
