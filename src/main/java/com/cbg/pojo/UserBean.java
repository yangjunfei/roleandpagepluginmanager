package com.cbg.pojo;

import com.cbg.Entity.Sex;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by chenboge on 2017/5/12.
 * <p>
 * Email:baigegechen@gmail.com
 * <p>
 * description:
 */
@Alias("user")
public class UserBean {

    private Integer id;

    private String userName;

    private Date birthday;

    private String mobile;

    private Sex sex;

    private String email;

    private String note;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
